﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuSceneManager : MonoBehaviour
{
    // Start is called before the first frame update
    public void PlayGame(bool vsAI)
    {
        PlayerPrefs.SetInt("vsAI", vsAI ? 1 : 0);
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
    }

    // Update is called once per frame
    public void QuitGame() {
        Application.Quit();
    }
}
