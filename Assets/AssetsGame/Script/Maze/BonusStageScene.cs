﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusStageScene : MonoBehaviour
{
    [SerializeField]MoveDestination character = null;

    [SerializeField] Transform goal = null;
    [SerializeField] Transform crystal = null;
    private void Start()
    {
        Vector3 pos = new Vector3(Random.Range(-2, 3),0, Random.Range(-4, 4));
        crystal.position = pos * 2;
        character.ReDestination(crystal.position);
    }
    // Update is called once per frame
    void Update()
    {
        if (crystal != null && character.isFinished) {
            character.ReDestination(goal.position);
            Destroy(crystal.gameObject);
        }
    }
}
