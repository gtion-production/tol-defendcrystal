﻿// MoveDestination.cs
using UnityEngine;
using UnityEngine.AI;

public class MoveDestination : MonoBehaviour
{
    public Animator anim = null;

    public NavMeshAgent agent = null;

    public bool isFinished {
        get {
            if (agent == null)
                return true;
            return agent.remainingDistance < agent.stoppingDistance; }
    }

    void Start()
    {
        anim.SetBool("Running",true);

    }

    public void ReDestination(Vector3 pos) {
        agent.destination = pos;
    }

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.transform.tag == "Goal")
        {
            Destroy(gameObject);
        }

    }
}