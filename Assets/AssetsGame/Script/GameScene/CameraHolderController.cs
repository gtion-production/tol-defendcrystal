﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CameraProjectionType
{
    Prespective ,Ortographic, AR
}

public class CameraHolderController : MonoBehaviour
{
    
    const float minX = -35;
    const float maxX = 0;

    [Header("Camera")]
    [SerializeField] CameraProjectionType projection = CameraProjectionType.Ortographic;
    [SerializeField] PerspectiveSwitcher dynamicCamera;
    [SerializeField] GameObject arCamera;
    int currentProjection = 0;

    //cange projection to next
    public void ChangeProjection() {

        currentProjection = (currentProjection+1)%3;
        Debug.Log("Change Projection : " + currentProjection);
        projection = (CameraProjectionType)currentProjection;

        ActivateCamera();
        
    }

    //on Screen scrolled
    public void DragCamera(Vector3 amount) {
        float deltaDrag = amount.y/10;

        Vector3 newEuler = transform.eulerAngles;
        newEuler.x = newEuler.x - Mathf.CeilToInt(newEuler.x / 360f) * 360f;
        newEuler.x = Mathf.Clamp(newEuler.x - deltaDrag, minX, maxX);
        transform.rotation = Quaternion.Euler(newEuler);
    }

    //change camera
    void ActivateCamera()
    {
        if ((projection == CameraProjectionType.Ortographic) || (projection == CameraProjectionType.Prespective))
        {
            GameSceneManager.CurrentCamera = dynamicCamera.GetComponent<Camera>();
            dynamicCamera.gameObject.SetActive(true);
            arCamera.SetActive(false);

            dynamicCamera.SwitchPrespective(projection == CameraProjectionType.Ortographic);
        }
        else {
            GameSceneManager.CurrentCamera = arCamera.GetComponent<Camera>();
            dynamicCamera.gameObject.SetActive(false);
            arCamera.SetActive(true);

            //prespectiveCamera.transform.position = arStartPos;

        }
    }

}
