﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class PointerEvent : UnityEvent<Vector3>
{

}

public class TouchScreen : MonoBehaviour, IPointerDownHandler , IPointerUpHandler , IDragHandler
{

    bool isClicking = false;

    //callback event
    //public delegate void OnClickDelegate(Vector3 pos);
    public PointerEvent onClick = null;

    public PointerEvent onDrag = null;

    Vector2 prevPos = Vector2.zero;

    public void rayCast(Vector2 TouchPos) {

        RaycastHit hit;
        //Debug.Log("Touch : "+TouchPos);
        if (GameSceneManager.CurrentCamera != null && Physics.Raycast( GameSceneManager.CurrentCamera.ScreenPointToRay(TouchPos), out hit, 100))
        {
            if (onClick != null)
            {
                onClick.Invoke(hit.point);
            }
        }

        
    }

    #region Pointer
    public void OnPointerDown(PointerEventData eventData)
    {
        prevPos = eventData.position;
        isClicking = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (isClicking)
        {
            rayCast(eventData.position);
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        isClicking = false;

        Vector3 delta = eventData.position - prevPos;
        prevPos = eventData.position;
        onDrag.Invoke(delta);
    }

    #endregion
}

