﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardController : MonoBehaviour
{
    public static BoardController Instance = null;

    [Header("Player Setup")]
    [SerializeField] Transform deffenderBase = null;
    [SerializeField] MeshRenderer[] deffenderMeshR = null;
    [SerializeField] Key crystal = null;

    [Header("Character Material")]
    //public bool isCurrentBlue = true;
    //public Material[] material = null;// 0  inactive , 1 blue , 2 red
    public Material[] deffenderMat = null;//0 blue , 1 red

    //rotate board
    Vector3 eulerRotationtarget = Vector3.zero;
    Vector3 dampVelocity = Vector3.zero;

    public Vector2 DeffenderBase
    {
        get {
            return new Vector2(deffenderBase.position.x, deffenderBase.position.z);
        }
    }

    void Start()
    {
        //create character collision ignorant to make attacker pass throught
        Physics.IgnoreLayerCollision(9, 11);
        Physics.IgnoreLayerCollision(11, 11);

        //singelton
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

    }

    public void Init(int deffenderIndex)
    {
        // 1 red deffender , 0 blue deffender

        crystal.SetNormalMaterial(deffenderMat[deffenderIndex]);

        for (int i = 0; i < deffenderMeshR.Length; i++)
        {
            deffenderMeshR[i].material = deffenderMat[deffenderIndex];
        }

        crystal.Restart();

    }

    public void RotateView(int index)
    {
        if (index == 0)
            eulerRotationtarget.y = 180;
        else
            eulerRotationtarget.y = 0;
    }

    private void Update()
    {
        Vector3 newEuler = Vector3.SmoothDamp(transform.eulerAngles, eulerRotationtarget, ref dampVelocity, 1f);
        transform.rotation = Quaternion.Euler(newEuler);
    }


}
