﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(GameSceneManager))]
public class AIEnemy : MonoBehaviour
{
    // simple enemy 
    public GameSceneManager manager;
    public StatusController controller;

    private void Start()
    {
        InvokeRepeating(nameof(AITakeAction), 10, 2);

        if (manager == null)
            manager.GetComponent<GameSceneManager>();
    }

    void AITakeAction()
    {
        Vector3 pos = new Vector3(Random.Range(-3.5f, 3.5f) , 0 , Random.Range(0.1f, 6.5f));
        
        manager.CreatePawn(pos, true);

        Debug.Log(pos);
    }
}
