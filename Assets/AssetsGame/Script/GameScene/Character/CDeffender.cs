﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CDeffender : CharacterBase
{
    const float SCAN_RADIUS = 3.675f; // 35% of map width (10.5)
    const float DEFAULT_COOLDOWN = 4;


    [SerializeField] SpriteRenderer areaDetectSprite = null;

    bool isTargetAvailable = false;
    Vector2 startPos = Vector3.zero;

    protected sealed override void Start()
    {
        base.Start();
        //save start position to comeback later
        startPos = new Vector2( transform.position.x , transform.position.z);

        type = CharacterType.Deffender;

        colorDirection.a = 0.1f;
        areaDetectSprite.material.SetVector("_OccludedColor", colorDirection);
    }

    protected sealed override void OnActivated()
    {
        base.OnActivated();
        speed = 1f;
    }
    protected sealed override void OnDeActivated()
    {
        isTargetAvailable = false;
        speed = 2f;
        base.OnDeActivated();
    }

    protected sealed override void CharacterActiveUpdate()
    {
        //if key holder is exist
        if (CAttacker.keyHolder != null)
        {
            //scane is holder in range?
            Vector3 delta = (CAttacker.keyHolder.position - transform.position);
            if (SCAN_RADIUS >= delta.magnitude)
            {
                Move(new Vector2(delta.x, delta.z));
                isTargetAvailable = true;
            }
            else
            {
                Move(Vector2.zero);
                isTargetAvailable = false;
            }
        }
        else if(isTargetAvailable)
        {
            Move(Vector2.zero);
            isTargetAvailable = false;
        }

    }

    protected sealed override void Update()
    {

        base.Update();
        
        //back to original position
        if (!isTargetAvailable && !isActive && cooldown < 2f)
        {
            Vector3 delta = (startPos - new Vector2(transform.position.x , transform.position.z));
            if(delta.magnitude > 0.1f)
                Move(delta);
            else
            {
                if (GameSceneManager.isBonusTime)
                {
                    //instant cooldown on bonus time
                    SetCoolDown(0);
                }
                else
                {
                    Move(Vector2.zero);
                }
            }
                
        }

        //delay comback for 2 second
        if (cooldown < 2f)
        {
            transform.position += direction * Time.deltaTime;
        }
    }


    protected override void SetCoolDown()
    {
        SetCoolDown(DEFAULT_COOLDOWN);
    }

    protected override void OnCollideEnemy()
    {
        base.OnCollideEnemy();
        animController.SetTrigger("Attack");
        animController.SetBool("Running", false);
    }

    private void OnDestroy()
    {
        Destroy(areaDetectSprite.gameObject);
    }

}
