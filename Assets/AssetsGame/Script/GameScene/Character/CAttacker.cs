﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CAttacker : CharacterBase
{
    #region static
    public static Transform keyHolder = null;//make key holder as static to easy to use
    public static List<CAttacker> Attackers = new List<CAttacker>();
    #endregion

    const float DEFAULT_COOLDOWN = 2.5F;

    //[IMPORTANT] if assigned to true the key will move to this object
    [SerializeField]bool _isHoldKey = false;

    //is this Character the one who hold the crystal key?
    public bool isHoldKey
    {
        get {
            return _isHoldKey;
        }
        set {
            _isHoldKey = value;

            //reduce speed when carrying Key
            if (_isHoldKey)
            {
                speed = (GameSceneManager.isBonusTime ? 2 : 1) * 0.75f;
                keyHolder = transform;
                gameObject.layer = LayerMask.NameToLayer("Character");
            }
            else
            {
                speed = 1.5f;
            }
        }
    }


    protected sealed override void Start()
    {
        base.Start();
        // initiate basic status
        type = CharacterType.Attacker;

        Attackers.Add(this);
    }

    
    protected sealed override void OnActivated()
    {
        if (Key.isOver != GameOverType.None)
            return;

        
        animController.SetBool("Running", true);
        base.OnActivated();

        //if this character is not key holder then ignore collision with enemies
        if(keyHolder != transform)
            gameObject.layer = LayerMask.NameToLayer("InnactiveCharacter");
    }
    
    protected sealed override void OnDeActivated()
    {
        //invoke delay animation
        Invoke(nameof(PlayHitAnim), 0.5f);
        animController.SetBool("Running", false);
        
        isHoldKey = false;
        base.OnDeActivated();
        graphicMaterial.material.color = Color.white;
    }

    void PlayHitAnim() {
        animController.SetBool("Active", false);
        graphicMaterial.material.color = Color.gray;
    }


    protected sealed override void CharacterActiveUpdate()
    {
        //move only if the game still running
        if (Key.isOver == GameOverType.None)
            transform.position += direction * Time.deltaTime;
        else
            return;

        //if holding key the straight to tower
        if (isHoldKey)
        {
            Vector2 target = BoardController.Instance.DeffenderBase;
            Vector2 myPos = new Vector2(transform.position.x, transform.position.z);
            Move(target - myPos);
        }
        else
        {
        //straigt foward
            if (eulerIdentityZero)
                Move(Vector2.up);
            else
                Move(Vector2.down);
        }

    }

    protected sealed override void SetCoolDown()
    {

        keyHolder = null; // set key holder to null, so the key will looking for nearest allies
        SetCoolDown(DEFAULT_COOLDOWN);
    }

    protected override void OnCollideEnvirontment(Collision collision)
    {
        base.OnCollideEnvirontment(collision);

        if (collision.transform.tag == "Border")
        {
            Destroy(gameObject);

            Instantiate(spawnEffect, transform.position, Quaternion.Euler(-90, 0, 0));
        } else if(collision.transform.tag == "Goal") {
            Vector3 delta = transform.position - Key.trans.position;

            if (new Vector2(delta.x, delta.z).magnitude <= 1)
            {
                //if success deliver crystal key to tower then win
                Key.isOver = GameOverType.AttackerWin;
                Debug.Log("Attacker Win");
            }
            Destroy(gameObject);


            Instantiate(spawnEffect, transform.position, Quaternion.Euler(-90,0,0));

        }

    }

    private void OnDestroy()
    {
        Attackers.Remove(this);
    }

   
}
