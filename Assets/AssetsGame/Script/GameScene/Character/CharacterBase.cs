﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CharacterType { 
    Attacker, Deffender
}

/*
Designed for online games
public enum CharacterOwner
{
    None,
    Myself, 
    Enemy
}
*/

public abstract class CharacterBase : MonoBehaviour
{

    [Header("Graphic")]
    [SerializeField] protected Animator animController = null;
    [SerializeField] protected SkinnedMeshRenderer graphicMaterial = null;
    [SerializeField] protected GameObject spawnEffect = null;

    [Header("Character Identifier")]
    [SerializeField] SpriteRenderer dirrectionSprite = null;
    [SerializeField] protected Color colorDirection = Color.white;

    protected CharacterType type = CharacterType.Attacker;

    protected MeshRenderer meshR = null;

    //Innactive time until reactivated
    protected float cooldown = 0.5f;

    //Movement data
    protected bool eulerIdentityZero = true;
    protected Vector3 direction = Vector3.zero;
    protected float speed = 1.5f;

    //variable to reduce Animation Set value
    bool prevActive = false;

    public bool isActive
    {
        get { return cooldown <= 0f; }
    }

    protected virtual void Start()
    {
        //set cooldown start at 0.5 + animation 0.2
        cooldown = 0.7f;

        meshR = GetComponent<MeshRenderer>();
        
        eulerIdentityZero = transform.eulerAngles.y == 0? true : false;
        dirrectionSprite.material.SetVector("_OccludedColor", colorDirection);
        Debug.Log("Char Summoned");

        Instantiate(spawnEffect, transform.position, Quaternion.Euler(-90, 0, 0));
    }

    //convert 2d direction to 3d world direction of character
    protected void Move(Vector2 direction) {
        
        direction.Normalize();
        if (direction == Vector2.zero)
        {
            transform.rotation = Quaternion.Euler(eulerIdentityZero ? Vector3.zero : Vector3.up * 180);
            animController.SetBool("Running", false);
        }
        else
        {
            transform.rotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.y));
            animController.SetBool("Running", true);
        }
        this.direction = new Vector3(direction.x , 0 , direction.y) * speed;
    }

    
    protected virtual void SetCoolDown(float time) {
        
        if (time <= 0 && !prevActive)
        {
            prevActive = true;
            
            OnActivated();//when active
        }
        else if (prevActive)
        {
            prevActive = false;
            
            OnDeActivated();//when innactive
        }

        cooldown = time;
    }

    /// <summary>
    /// OnCharacter Activated
    /// </summary>
    protected virtual void OnActivated() {
        
        animController.SetBool("Active", true);
        gameObject.layer = LayerMask.NameToLayer("Character");
        graphicMaterial.material.color = Color.white;
    }

    /// <summary>
    /// OnCharacter De-Activated
    /// </summary>
    protected virtual void OnDeActivated()
    {
        Debug.Log("Non Active : " + name);
        gameObject.layer = LayerMask.NameToLayer("InnactiveCharacter");
        graphicMaterial.material.color = Color.gray;
    }

    protected virtual void Update()
    {

        if (!isActive)
        {
            //if innactive the reduce cooldown to reactivate
            SetCoolDown( cooldown - Time.deltaTime);
        }
        else
        {
            //action taken by character when active
            CharacterActiveUpdate();
        }

        /*
        if (Input.GetKeyDown(KeyCode.Space))
            SetCoolDown(1f);
        */
    }


    protected void OnCollisionEnter(Collision collision)
    {
        CharacterBase enemy = collision.gameObject.GetComponent<CharacterBase>();
        if (enemy != null)
        {
            if (enemy.type != type)
            {
                OnCollideEnemy();
            }
            
        }
        else
        {
            OnCollideEnvirontment(collision);
        }
    }

    /// <summary>
    /// Called once collided with enemy
    /// </summary>
    protected virtual void OnCollideEnemy() {
        SetCoolDown();
    }

    /// <summary>
    /// Called once collided with Environtments
    /// </summary>
    /// <param name="collision"></param>
    protected virtual void OnCollideEnvirontment(Collision collision)
    {}

    /// <summary>
    /// Run main functionality of a character
    /// </summary>
    protected abstract void CharacterActiveUpdate();

    /// <summary>
    /// Default time cooldown
    /// </summary>
    protected abstract void SetCoolDown();

}
