﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSceneManager : MonoBehaviour
{
    public static Camera CurrentCamera = null;
    public static bool isBonusTime = false;
    const int MAX_ROUND = 3;//minimal round , if round is exceed counted as bonus round and get 15 sec bonus time

    [Header("Input")]
    [SerializeField] WinLoseUIController nextRoundcontroller = null;//UI controller after a match

    [Header("Object")]
    [SerializeField] Transform characterParent = null;// root of all character

    [Header("Game Config")]
    public bool blueAttacker = true;
    [SerializeField] bool againstAI = true;

    [Header("Player")]
    [SerializeField] StatusController[] controller = null;
    [SerializeField] Timer timer = null;

    [Header("Prefabs")]
    [SerializeField] GameObject blueChar = null;
    [SerializeField] GameObject redChar = null;
    [SerializeField] BoardController board = null;

    //match data
    int playerBlueScore = 0;
    int playerRedScore = 0;
    int roundCount = 0;
    bool bonusTimeUsed = false;

    //accessor index
    public int AttackerIndex {
        get { return blueAttacker? 0 : 1; }
    }
    public int DeffenderIndex
    {
        get { return blueAttacker ? 1: 0; }
    }


    private void Start()
    {
        //load game mode
        againstAI = PlayerPrefs.GetInt("vsAI" )==1 ;

        //swap UI for PvAI and PvPlayer
        if (!againstAI)
        {
            StatusController temp = controller[2];
            controller[2] = controller[1];
            controller[1] = temp;

            controller[1].gameObject.SetActive(true);
            controller[2].gameObject.SetActive(false);
        }

        //set regen to zero when intro running
        SetBonusRegen(0);

        //initialize board
        board.Init(DeffenderIndex);

        //waiting for intro, then start the game
        Invoke(nameof(GameStart), 10);

        //destroy AI script if PVP
        AIEnemy ai = GetComponent<AIEnemy>();
        if (againstAI)
            ai.controller = controller[1];
        else
            Destroy(ai);
        
    }

    void GameStart() {

        //add round counter
        roundCount++;
        isBonusTime = false;

        //Hide all UI when the game start
        nextRoundcontroller.HideAll();

        //Reset player and enemy status and bar count
        controller[AttackerIndex].initRole(true);
        controller[DeffenderIndex].initRole(false);

        //Set energy regen to *1
        SetBonusRegen(1);

        //Set up View
        if (!blueAttacker)
        {
            //rotate board at 180 degree
            board.RotateView(0);
        }
        else {
            //rotate board at 0 degree
            board.RotateView(1);
        }

        //Destroy all previous character
        foreach (Transform c in characterParent) {
            Destroy(c.gameObject);
        }
        CAttacker.Attackers.Clear();

        //initialize board
        board.Init(DeffenderIndex);

        //set game time for 60 second
        timer.SetTime(60);
    }

    void SetBonusRegen(int bonus) {
        for (int i = 0; i < controller.Length; i++)
        {
            controller[i].BonusRegen = bonus;
        }
    }

    //Create Pawn based on location 'pos'
    public void CreatePawn(Vector3 pos) {
        CreatePawn(pos, !againstAI);
    }
    public void CreatePawn(Vector3 pos , bool isAI)
    {
        //dont instantiate if not in the ground
        if (pos.y > 1)
        {
            Debug.Log("cant instantiate from not ground object");
            return;
        }

        //dont instantiate if the match is over
        if (GameOverType.None != Key.isOver) {
            Debug.Log("Can't Spawn, Game is Over");
            return;
        }

        
        if (pos.z > 0 &&  isAI ) //AI summon area
        {
            if (blueAttacker)
                SummonDeffender(pos);
            else
                SummonAttacker(pos);
            
        }
        else if(pos.z < 0 ) //player summon area
        {
            if (blueAttacker)
                SummonAttacker(pos);
            else
                SummonDeffender(pos);
        }

        
    }

    void SummonDeffender(Vector3 pos) {
        // cost 3 bar to summon defender
        if (controller[DeffenderIndex].UserBar(3))
        {
            GameObject temp = null;
            if (blueAttacker)
            { //if blue attacker then red is deffender
                temp = Instantiate(redChar, pos + new Vector3(0, 1, 0), Quaternion.Euler(0,180,0), characterParent);
            }
            else
            {
                temp = Instantiate(blueChar, pos + new Vector3(0, 1, 0), Quaternion.identity, characterParent);

            }
            
            Destroy(temp.GetComponent<CAttacker>());
        }
    }
    void SummonAttacker(Vector3 pos)
    {
        //cost 2 bar to summon attacker
        if (controller[AttackerIndex].UserBar(2))
        {
            GameObject temp = null;
            if (blueAttacker)
            { //if blue attacker then red is deffender
                temp = Instantiate(blueChar, pos + new Vector3(0, 1, 0), Quaternion.identity, characterParent);
            }
            else
            {
                temp = Instantiate(redChar, pos + new Vector3(0, 1, 0), Quaternion.Euler(0, 180, 0), characterParent);
            }
            
            Destroy(temp.GetComponent<CDeffender>());
        }
    }

    /// <summary>
    /// Callback event on timer is already 0
    /// </summary>
    public void TimesUp()
    {
        if ((MAX_ROUND >= roundCount) || bonusTimeUsed)
        {
            
            if (Key.isOver == GameOverType.None) {
                //when no one winning this match and the time is over then the defender is the winner
                Debug.Log("Deffender Win1");
                Key.isOver = GameOverType.DeffenderWin;
            }
            GameOver();
        }
        else {
            //add 15 second *2 regen bonus when round is greather than it should be
            bonusTimeUsed = true;
            isBonusTime = true;
            timer.AddTime(15);
            SetBonusRegen(2);
        }
    }

    public void NextRound() {
        //avoid multiple click
        if (roundCount != playerBlueScore + playerRedScore)
            return;

        //resetting value
        bonusTimeUsed = false;
        //swap position
        blueAttacker = !blueAttacker;

        GameStart();
    }

    public void GameOver() {

        //add score to whose win match
        bool isBlueWin = false;
        if (Key.isOver == GameOverType.AttackerWin)
        {
            if (blueAttacker)
            {
                playerBlueScore++;
                isBlueWin = true;
            }
            else
            {
                playerRedScore++;
                isBlueWin = false;
            }
            
        }
        else {
            if (blueAttacker)
            {
                playerRedScore++;
                isBlueWin = false;
            }
            else
            {
                playerBlueScore++;
                isBlueWin = true;
            }
        }

        //update score on UI
        controller[AttackerIndex].SetScore(blueAttacker ? playerBlueScore : playerRedScore);
        controller[DeffenderIndex].SetScore(!blueAttacker ? playerBlueScore : playerRedScore);

        Debug.Log("Blue : " + playerBlueScore + " Red : " + playerRedScore);

        
        if (MAX_ROUND >= roundCount || roundCount % 2 != 0 || playerBlueScore == playerRedScore)
        {
            //next game if the round is less the requirement or round is odd or the score is draw
            nextRoundcontroller.OpenNextGame(isBlueWin);
        }
        else {
            //open Final UI, show whose the winner
            nextRoundcontroller.OpenFinalGame(playerBlueScore > playerRedScore);
            /*
            if (playerBlueScore > playerRedScore)
            {
                Debug.Log("Blue Win");
            }
            else {
                Debug.Log("Red Win");
            }
            */
        }
    }

    //back to main menu
    public void BackToHome() {
        Debug.Log("BackToHome");
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

}
