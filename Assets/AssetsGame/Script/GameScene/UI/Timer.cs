﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{

    [SerializeField]UnityEngine.Events.UnityEvent OnTimeUp;

    [SerializeField] int time = 60;
    [SerializeField] UnityEngine.UI.Text text = null;

    bool timeUpInvoked = false;

    // Start is called before the first frame update
    void Start()
    {
        timeUpInvoked = false;
        InvokeRepeating(nameof(TimeUpdate), 10, 1);
    }

    public void AddTime(int value)
    {
        SetTime(time + value);
    }

    public void SetTime(int value)
    {
        timeUpInvoked = false;
        time = value;
        text.text = time.ToString();
    }

    // Update is called once per frame
    void TimeUpdate()
    {
        if ((GameOverType.None != Key.isOver || time == 0) && !timeUpInvoked)
        {
            timeUpInvoked = true;
            Debug.Log("Time Up Invoked");
            OnTimeUp.Invoke();
            return;
        }

        if (!timeUpInvoked)
        {
            time = Mathf.Max(0, time - 1);
            text.text = time.ToString();
        }
    }
}
