﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinLoseUIController : MonoBehaviour
{
    // script for animation controller UI

    [Header("Text")]
    [SerializeField]Text blueResult = null;
    [SerializeField] Text redResult = null;
    [SerializeField] Text finalResult = null;

    [Header("Animation")]
    [SerializeField] Animation nextBtnAnim = null;
    [SerializeField] Animation blueAnim = null;
    [SerializeField] Animation redAnim = null;
    [SerializeField] Animation finalAnim = null;

    [Header("Image")]
    [SerializeField] Image finalImage = null;
    [SerializeField] Color colorBlue = Color.white;
    [SerializeField] Color colorRed = Color.white;

    bool isOpen = false;

    public void OpenFinalGame(bool isblueWin)
    {
        if (isblueWin) 
            finalImage.color = colorBlue;
        else
            finalImage.color = colorRed;

        finalResult.text = isblueWin ? "BLUE" : "RED";
        finalAnim.Play("YWin");
    }

    public void OpenNextGame(bool isblueWin)
    {
        isOpen = true;
        if (isblueWin)
        {
            blueResult.text = "WIN!";
            redResult.text = "LOSE!";
        }
        else {
            blueResult.text = "LOSE!";
            redResult.text = "WIN!";
        }


        nextBtnAnim.Play("Show");
        blueAnim.Play("YWin");
        redAnim.Play("YWin");
    }

    public void HideAll() {
        if (!isOpen)
            return;
        nextBtnAnim.Play("Hide");
        blueAnim.Play("YClose");
        redAnim.Play("YClose");
    }

    
}
