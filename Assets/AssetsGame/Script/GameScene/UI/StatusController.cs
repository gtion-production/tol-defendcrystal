﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatusController : MonoBehaviour
{
    const float BAR_COUNT = 6;
    const float REGENERATION_SPEED = 0.5f;
    //const float BAR_COOLDOWN = 0.5f;
    //const string DELETE_BAR = "DeleteBar";

    [SerializeField] Text roleName = null;
    [SerializeField] Text textScore = null;
    [SerializeField] Text textAmount = null;
    [SerializeField] Image barFiller = null;
    [SerializeField] Animation[] bar = null;

    [SerializeField]int currentBar = 0;

    int _bonusRegen = 1;
    public int BonusRegen {
        set { 
            _bonusRegen = Mathf.Max( 0 , value); 
        }
    }

    public int BarAmount {
        get {
            return Mathf.FloorToInt( barFiller.fillAmount * BAR_COUNT );
        }
    }

    /// <summary>
    /// reset all value
    /// </summary>
    /// <param name="isAttacker"></param>
    public void initRole(bool isAttacker) {
        roleName.text = isAttacker ? "Attacker" : "Deffender";
        textAmount.text = BarAmount.ToString();
        
        UserBar(BarAmount);
        currentBar = 0;
    }

    public void SetScore(int value) {
        textScore.text = value.ToString();
    }

    public bool UserBar(int value) {

        if (BarAmount >= value)
        {

            barFiller.fillAmount -= value / BAR_COUNT;

            for (int i = 0; i < value; i++)
            {
                DeleteBar();
            }
            textAmount.text = BarAmount.ToString();
            return true;
        }
        return false;
    }

    void DeleteBar() {
        currentBar--;
        bar[currentBar].Play("Delete");
    }
    void AddBar(){
        bar[currentBar].Play("Add");
        currentBar++;
        textAmount.text = BarAmount.ToString();
    }

    void Update()
    {
        barFiller.fillAmount += _bonusRegen * REGENERATION_SPEED * Time.deltaTime/ BAR_COUNT;
        if (BarAmount > currentBar) {
            AddBar();
        }

        //if (Input.GetKeyDown(KeyCode.Space) && UserBar(2));

    }
}
