﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameOverType { 
    None,
    AttackerWin,
    DeffenderWin
}

public class Key : MonoBehaviour
{
    const float HEIGHT_FINAL = 2.4f;
    const float HEIGHT_HALF = 0f;
    const float HEIGHT_NORMAL = -1f;

    //give an 
    static bool isFirst = true;
    public static Transform trans = null;
    public static GameOverType isOver = GameOverType.None;

    //material on innactive and active, Must 2 element
    [SerializeField] Material[] material = null;
    MeshRenderer mesh = null;

    [SerializeField]float ROTATION_SPEED = 40;
    float speed = 1.5f;
    bool IsHolded {
        get { return CAttacker.keyHolder != null; }
    }
    //prevent some event called twice
    bool prevHolded = false;

    private void Awake()
    {
        trans = transform;
        mesh = GetComponent<MeshRenderer>();
    }

    /// <summary>
    /// Reset battle result
    /// </summary>
    public void Restart() {
        Debug.Log("Game Restarted");
        isFirst = true;
        isOver = GameOverType.None;
    }

    //change color according the winner
    public void SetNormalMaterial(Material normal) {
        material[0] = normal;
        mesh.material = material[0];
    }

    //get attacker from nearest distacnce
    CAttacker GetNearestAttacker() {
        
        if (CAttacker.Attackers.Count == 0)
            return null;

        //searching nearest active attacker
        CAttacker[] attackers = CAttacker.Attackers.ToArray();
        int index = -1;
        float minDist = float.PositiveInfinity;
        for (int i = 0; i < attackers.Length; i++) {
            
            float dist = Vector2.Distance(attackers[i].transform.position, transform.position);
            if (attackers[i].isActive && minDist > dist)
            {
                minDist = dist;
                index = i;
            }
        }
        return index == -1 ? null : attackers[index];
    }


    void Update()
    {
        //rotate z angle
        Vector3 euler = transform.rotation.eulerAngles;
        euler.y += ROTATION_SPEED * Time.deltaTime;
        transform.rotation = Quaternion.Euler(euler);

        //if the game is over then go to tower
        if (isOver != GameOverType.None)
        {
            Vector2 delta = BoardController.Instance.DeffenderBase;
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(delta.x, HEIGHT_FINAL, delta.y), 3 * Time.deltaTime);

            return;
        }

        
        if (!IsHolded)
        {
            //change material once
            if (IsHolded != prevHolded)
            {
                mesh.material = material[0];
                prevHolded = IsHolded;
            }


            bool isHolderExist = false;
            CAttacker attacker = GetNearestAttacker();

            if (attacker != null)
            {
                //Debug.Log("BBB");
                isFirst = false;
                isHolderExist = true;
                attacker.isHoldKey = true;
            }else if (isFirst) {
                //Debug.Log("AAA");
                isHolderExist = true;
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(0, HEIGHT_NORMAL , 0), 10 * Time.deltaTime);
            }

            //the holder exit but no one can hold any longer then the defender win
            if (!isHolderExist)
            {
                Debug.Log("Deffender Win2");
                isOver = GameOverType.DeffenderWin;
            }
        }
        else
        {
            //move to holder position
            Vector3 target = CAttacker.keyHolder.position;

            Vector3 delta = (transform.position - target);
            delta.y = 0;
            if (delta.magnitude < 0.5f)
            {
                target.y = HEIGHT_HALF;
                if (IsHolded != prevHolded)
                {
                    mesh.material = material[1];
                    prevHolded = IsHolded;
                }
            }
            else
            {
                target.y = HEIGHT_NORMAL;
            }

            transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
        }


        

    }
}
