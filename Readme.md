# Tower of Lune: Defend Crystal
![App Icon](Assets/AssetsGame/Image/IconTOL.png)

### Disclaimer
The game created may not be 100% in accordance with the file sent.
because there are some design changes in order to provide original work, so some requirements must be changed or deleted.


### Tag Description
DONE : Selesai

UNDONE : Tidak sslesai (tidak diimplementasi karena *reason)

ALTERNATIVE : Tidak bisa diselesaikan, menggunakan alternative lain (*Reason and Solution)

TODO : masi dikerjakan

### Task Lists
---
#### Gameplay Basic 1
##### Part 1
* [DONE] 3D Unity Project
* [DONE] Graphic: Battle Field, Ball, Soldiers, Timebox, EnergyBars...
* [DONE] Control: Tap on Battle Fields
* [DONE] Camera: Orthographic

##### Part 2
* [DONE] Resolve Game End

##### Part 3
* [DONE] Indicators
	- [Ok] Direction of Soldiers (Arrow)
	- [Ok] Highlight of Attacker holding the Ball (Crystal Light)
* [DONE] Solve the collision between the attackers and defenders

##### Part 4
* [DONE] Animation when the Defender caught the Attacker (for both)
* [DONE] Animation for the Defender Vision
* [ALTERNATIVE] Animation when the Soldier is Spawn (unsolve able due to limitation, instead particle effects)

##### Part 5
* [ALTERNATIVE] Animation when the Attacker is destroyed at opponent’s Fence(unsolve able due to limitation, instead particle effects)

---
#### AR Mode Basic 2
* [DONE] Able to switch between normal and AR mode
* [UNDONE] Able to detect surface and display the game on it
(*The idea not reached, seems doesnt make sense if the board position became unconsistent based on surface, moreover surface sometimes not detected may cause problem in fast switching )
* [UNDONE] Complete basic requirements defined on Unity_Test_AR_Ref.pdf 
(*Dont have the files)

---
#### Penalty Map
* [DONE]Generate the Maze successfully
* [DONE]The Ball is placed randomly at place that can be get (Example image only)
* [DONE]The Maze can be solved (has way out)
* NOTE :  Not installed on game loop, check `Assets/AssetsGame/Scene/BonusStageScene`

---
#### Gameplay Bonus 2
##### Part 1
* [DONE] Using Model to replace to Ball, Attackers and Defenders...

##### Part 2
* [DONE] Particles effects for:
	- [Ok] Gate/Fence
	- [Ok] Soldier in countdown to be reactivated
	- [Ok] The Defender Detection Circle

##### Part 3
* [TODO] Menu system
	- [Ok] Main Menu
	- [Ok] In Game Menu
	- [Ok] Win-Lose Screens

##### Part 4	
* [DONE] Extra gameplay Rush Time: for last 15s
	- [Ok] The energy regenerate faster
	- [Ok] Defender is now reactivated when back to it position
	- [Ok] The Attacker that carry the Ball will not move slower

##### Part 5
* [UNDONE] Sound BGM/SFX
* [ALTERNATIVE] Use cinemachine to make Intro
(* There are no special camera effects that really need cinemachine so adding a plugin would be useless, using animation instead)

---
#### Gameplay Bonus 2
##### Part 1
* [ALTERNATIVE] Has Pinch zoom and Swipes horizontally
(* Instead swipes vertically only)
* [ALTERNATIVE] Display Shadow for Game board
(* unfortunately im using bloom effect which is the background must be black to create glow effect. instead character shadow still exist)
* [DONE] Using Universal Render Pipeline

